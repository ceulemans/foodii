# built ssr assets with npm and vite
FROM node:lts-alpine AS node
WORKDIR /app

COPY /package.json /package-lock.json /app/
RUN npm ci

COPY /resources/ /app/resources/

COPY /vite.config.ts /postcss.config.js /tailwind.config.js /app/
RUN npm run build

# build ssr image
FROM node:lts-alpine
WORKDIR /app

# copy vite assets to nginx image
COPY --from=node /app/bootstrap/ssr/ /app/bootstrap/ssr/
COPY --from=node /app/node_modules/ /app/node_modules/

CMD ["node", "bootstrap/ssr/ssr.mjs"]

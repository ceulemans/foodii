# Composer installation
FROM php:8.2-fpm-alpine AS composer
WORKDIR /app

## install composer dependencies
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions @composer
COPY / /app
RUN composer install --optimize-autoloader --no-dev

# built frontend assets with npm and vite
FROM node:lts-alpine AS node
WORKDIR /app

COPY /package.json /package-lock.json /app/
RUN npm ci

COPY /resources/ /app/resources/

COPY /vite.config.ts /postcss.config.js /tailwind.config.js /app/
RUN npm run build

# build php-fpm image
FROM php:8.2-fpm-alpine
WORKDIR /app

# install required extensions
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions bcmath pdo_pgsql redis

# copy application source code
COPY / /app

# copy vite assets to nginx image
COPY --from=node /app/bootstrap/ssr/ /app/bootstrap/ssr/
COPY --from=node /app/public/build/ /app/public/build/

# copy dependencies from composer installation step
COPY --from=composer /app/vendor/ /app/vendor/

# make storage writable for application
RUN chown --recursive www-data:www-data /app/storage

# optimize application before start
COPY /.docker/php/entrypoint.sh /usr/bin/entrypoint.sh

ENTRYPOINT ["/usr/bin/entrypoint.sh"]

CMD ["php-fpm"]

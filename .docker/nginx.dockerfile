# built frontend assets with npm and vite
FROM node:lts-alpine AS node
WORKDIR /app

COPY /package.json /package-lock.json /app/
RUN npm ci

COPY /resources/ /app/resources/

COPY /vite.config.ts /postcss.config.js /tailwind.config.js /app/
RUN npm run build

FROM nginx:1-alpine

WORKDIR /app/public

# copy vite assets to nginx image
COPY --from=node /app/public/build/ /app/public/build/

COPY /public/ /app/public/

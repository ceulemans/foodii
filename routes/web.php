<?php

use Illuminate\Support\Facades\Route;
use Spatie\Health\Http\Controllers\HealthCheckResultsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return inertia('Home');
});

Route::get('/order', function () {
    $dishes = app(\App\Domains\Dish\Repositories\DishRepository::class)->all();

    return inertia('OrderFood', [
        'dishes' => $dishes,
    ]);
});

Route::get('dish/{id}', function (string $id) {
    $dish = app(\App\Domains\Dish\Repositories\DishRepository::class)->get($id);

    return inertia('Dishes/DishDetails', [
        'dish' => $dish,
    ]);
});

Route::get('/contact', function () {
    return inertia('Contact');
});

Route::post('/contact', function () {
    $data = request()->validate([
        'name' => ['required'],
        'email' => ['required', 'email'],
        'content' => ['required', 'max:200'],
    ]);

    app(\App\Domains\Feedback\Actions\SaveFeedbackAction::class)(
        name: $data['name'],
        email: $data['email'],
        content: $data['content'],
    );

    return back()
        ->with('flash.info', 'Thank you for submitting your feedback!');
});

Route::get('/headers', function () {
    return response()->json(request()->header());
});

Route::get('health', HealthCheckResultsController::class);

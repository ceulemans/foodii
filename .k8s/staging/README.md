# Setting up staging

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add jetstack https://charts.jetstack.io

helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx --version "4.4.2" \
  -n ingress-nginx --create-namespace \
  -f .k8s/staging/values/nginx-ingress-values.yaml

# Grab IP using:
kubectl get svc -n ingress-nginx
# Point domain to Load Balancer

helm install cert-manager jetstack/cert-manager --version "v1.11.0" \
  -n cert-manager --create-namespace \
  -f .k8s/staging/values/cert-manager-values.yaml

kubectl apply -k .k8s/staging
```

import { createInertiaApp } from "@inertiajs/vue3"
// @ts-ignore
import createServer from "@inertiajs/vue3/server"
import { renderToString } from "@vue/server-renderer"
import { createSSRApp, DefineComponent, h } from "vue"
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers"

createServer((page) =>
    createInertiaApp({
        page,
        render: renderToString,
        title: (title) => `${title} - Foodii`,
        resolve: (name) =>
            resolvePageComponent(
                `./Pages/${name}.vue`,
                import.meta.glob("./Pages/**/*.vue")
            ) as Promise<DefineComponent>,
        setup({ App, props, plugin }) {
            return createSSRApp({
                render: () => h(App, props),
            }).use(plugin)
        },
    })
)

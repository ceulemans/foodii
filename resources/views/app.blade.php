<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8" />
    <meta
        name="description"
        content="Foodii is a ordering system for takeaway restaurants, with a focus on being affordable and taking the smallest cut possible from restaurant's revenue."
    />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @vite('resources/ts/app.ts') @inertiaHead
</head>
<body class="bg-gray-50 text-black dark:bg-gray-900 dark:text-white">
@inertia
</body>
</html>

<?php

namespace App\Domains\Dish\Repositories;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DishRepository
{
    private array $dishes;

    public function __construct()
    {
        $this->dishes = array_merge(...array_fill(0, 5, [
            [
                'id' => md5(1),
                'name' => 'Babi Panggang',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/ngg_featured/BabiPangang.png',
            ],
            [
                'id' => md5(2),
                'name' => 'Nasi Goreng',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/ngg_featured/nasi-goreng.png',
            ],
            [
                'id' => md5(3),
                'name' => 'Foo Young Hai',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/Foo-Young-Hai-562x400.png',
            ],
            [
                'id' => md5(4),
                'name' => 'Bok Choy',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/ngg_featured/Bok-Choy.png',
            ],
            [
                'id' => md5(5),
                'name' => 'Eggplant Parmesan',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/ngg_featured/Egg-Plant-Parmesan.png',
            ],
            [
                'id' => md5(6),
                'name' => 'Sweet Potato Quiche',
                'image' => 'https://arrisje.com/wp/wp-content/uploads/ngg_featured/sweetpotatocrustquiche.png',
            ],
        ]));
    }

    public function all(): array
    {
        return $this->dishes;
    }

    public function get(string $id): array
    {
        return collect($this->dishes)
            ->firstWhere('id', $id)
            ?? throw new NotFoundHttpException();
    }
}

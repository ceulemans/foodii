<?php

declare(strict_types=1);

namespace App\Domains\Feedback\Actions;

use App\Domains\Feedback\Repositories\FeedbackRepository;

readonly class SaveFeedbackAction
{
    public function __construct(private FeedbackRepository $repository)
    {
    }

    public function __invoke(string $name, string $email, string $content): void
    {
        $this->repository->save($name, $email, $content);
    }
}

<?php

declare(strict_types=1);

namespace App\Domains\Feedback\Repositories;

use App\Models\Feedback;

class FeedbackRepository
{
    public function save(string $name, string $email, string $content): void
    {
        Feedback::query()->create([
            'name' => $name,
            'email' => $email,
            'content' => $content,
        ]);
    }
}
